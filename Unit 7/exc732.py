def check_win(secret_word, old_letters_guessed):
    """
    Checks if the player succeed to win the game session
    :param secret_word: The value the player need to guess.
    :param old_letters_guessed: All the attempts the user had ever tried during the game.
    :type secret_word: String
    :type old_letters_guessed: list
    :return: True if the player wins, otherwise False
    """
    win_length = len(secret_word)
    current_win_status = 0
    for letter in old_letters_guessed:
        if letter in secret_word:
            for letter2 in secret_word:
                if letter == letter2:
                    current_win_status += 1
    return current_win_status == win_length


def main():
    secret_word = "yes"
    old_letters_guessed = ['d', 'g', 'e', 'i', 's', 'k', 'y']
    print(check_win(secret_word, old_letters_guessed))


if __name__ == '__main__':
    main()
