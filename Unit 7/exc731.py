def show_hidden_word(secret_word, old_letters_guessed):
    """
    Creates a String which display the user when he did correct moves so far.
    :param secret_word: The value the player need to guess.
    :param old_letters_guessed: All the attempts the user had ever tried during the game.
    :type secret_word: String
    :type old_letters_guessed: list
    :return: The current status of the player.
    :rtype: String
    """
    status_lst = []
    for i in range(len(secret_word)):
        status_lst.append('_')
    for letter in old_letters_guessed:
        if letter in secret_word:
            idx = 0
            for letter2 in secret_word:
                if letter == letter2:
                    status_lst[idx] = letter
                idx += 1
    return " ".join(status_lst)


def main():
    secret_word = "mammals"
    old_letters_guessed = ['s', 'p', 'j', 'i', 'm', 'k']
    print(show_hidden_word(secret_word, old_letters_guessed))


if __name__ == '__main__':
    main()