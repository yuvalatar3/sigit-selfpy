def main():
    count = 0
    market = input("Enter Your list here:")
    market_lst = market.split(",")
    operation = int(input("Now decide what you want to do.\nChoose a number between 1 - 9."))
    if operation == 1:
        for item in market_lst:
            print(item)
    elif operation == 2:
        print("There are ", len(market_lst), " items in the list.")
    elif operation == 3:
        mutzar = input("Please enter something you want to check")
        print(mutzar in market_lst)
    elif operation == 4:
        mutzar = input("Please enter something you want to check: ")
        for item in market_lst:
            if item == mutzar:
                count += 1
        print("The item is ", count, " times in the list.")
    elif operation == 5:
        mutzar = input("Please enter something you want to check: ")
        market_lst.remove(mutzar)
        print("The updated list is:")
        for item in market_lst:
            print(item)
    elif operation == 6:
        mutzar = input("Please enter something you want to check: ")
        market_lst.append(mutzar)
        print("The updated list is:")
        for item in market_lst:
            print(item)
    elif operation == 7:
        for item in market_lst:
            if len(item) < 3 or not(item.isalpha()):
                print(item)
    elif operation == 8:
        for item in market_lst:
            count = 0
            for sec_item in market_lst:
                if sec_item == item:
                    count += 1
                    if count > 1:
                        market_lst.remove(sec_item)
        print("The updated list is:")
        for item in market_lst:
            print(item)
    elif operation == 9:
        print("Goodbye :)")


if __name__ == '__main__':
    main()