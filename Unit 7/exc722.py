def numbers_letters_count(my_str):
    """
    Checks how many digits are in passed String and how many characters are in the String which aren't digits.
    :param my_str: Any string value.
    :type: str
    :return: Amount of the digits in passed String and amound of the characters that aren't digits.
    :rtype: list
    """
    digit_count = 0
    others_count = 0
    summerized_list = []
    for char in my_str:
        if char.isdigit():
            digit_count += 1
        else:
            others_count += 1
    summerized_list.append(digit_count)
    summerized_list += [others_count]
    return summerized_list

def main():
    result = numbers_letters_count("Python 3.6.3")
    print(result)


if __name__ == '__main__':
    main()
