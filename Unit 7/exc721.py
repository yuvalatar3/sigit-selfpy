def is_greater(my_list, n):
    """
    Checks which numbers in the list are greater then the n number.
    :param my_list: Collection of numbers
    :param n: any numeric value.
    :type my_list: list
    :type n: int
    :return: all the numbers in passed list which greater then the passed n parameter.
    :rtype: list
    """
    best_lst = []
    for num in my_list:
        if num > n:
            best_lst.append(num)
    return best_lst


def main():
    result = is_greater([1, 30, 25, 60, 27, 28], 28)
    print(result)


if __name__ == '__main__':
    main()
