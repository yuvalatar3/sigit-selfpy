def arrow(my_char, max_length):
    """
    Creates a good looking arrow
    :param my_char: The pieces the arrow will be build from.
    :param max_length: The length of the top of the arrow.
    :type my_char: String
    :type max_length: int
    :return: The best arrow in the world.
    """
    best_arrow = ""
    for i in range(1, max_length+1):
        best_arrow += (my_char + ' ') * i + "\n"
    for i in range(max_length - 1, 0, -1):
        best_arrow += (my_char + ' ') * i + "\n"
    return best_arrow


def main():
    result = arrow('*', 5)
    print("\nArrow created:\n\n", result)


if __name__ == '__main__':
    main()
