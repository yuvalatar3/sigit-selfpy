def squared_numbers(start, stop):
    """
    Display the squared values of the numbers between the start and stop range.
    :param start: some number.
    :param stop:  some other number, bigger than start. (No need to check it)
    :type start: int
    :type stop: int
    :return: List of all the squared numbers between the start and stop range.
    :rtype: list
    """
    i = start
    sqrt_numbers = []
    while i <= stop:
        sqrt_numbers.append(i ** 2)
        i += 1
    return sqrt_numbers


def main():
    print(squared_numbers(-3, 3))


if __name__ == '__main__':
    main()
