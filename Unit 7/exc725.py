def sequence_del(my_str):
    """
    Remove all the characters which are in a row one each after in passed String.
    :param my_str: Some letters.
    :type my_str: String
    :return: fixed String
    :rtype: String
    """
    best_str = ""
    seq_let = ''
    idx = 0
    golden_index = 0
    for letter in my_str:
        idx += 1
        if letter != seq_let:
            golden_index = idx - 1
            best_str += letter
            seq_let = letter
        else:
            continue
    return best_str


def main():
    result = sequence_del("ppyyyyythhhhhooonnnnn")
    print(result)
    result = sequence_del("SSSSsssshhhh")
    print(result)
    result = sequence_del("Heeyyy   yyouuuu!!!")
    print(result)


if __name__ == '__main__':
    main()
