def seven_boom(end_number):
    """
    Simulating the game of 'Seven Boom'
    :param end_number: The value we want to stop the game at.
    :type end_number: int
    :return: All the numbers which aren't divide by 7 or contain the digit 7.
    :rtype: list
    """
    no_sevens_lst = []
    for number in range(end_number + 1):
        if '7' in str(number) or number % 7 == 0:
            no_sevens_lst.append("BOOM")
        else:
            no_sevens_lst.append(number)
    return no_sevens_lst


def main():
    result = seven_boom(17)
    print(result)


if __name__ == '__main__':
    main()
