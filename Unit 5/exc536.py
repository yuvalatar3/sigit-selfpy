def filter_teens(a=13, b=13, c=13):
    a = fix_age(a)
    b = fix_age(b)
    c = fix_age(c)
    return a + b + c


def fix_age(age):
    if age > 12 and age < 20 and age != 15 and age != 16:
        return 0
    return age


if __name__ == '__main__':
    print(filter_teens(2, 1, 15))
