def distance(num1, num2, num3):
    return (abs(num3 - num1) == 1 or abs(num2 - num1) == 1) and ((abs(num3-num1) > 1 and abs(num3-num2) > 1) or (abs(num2-num1) > 1 and abs(num2-num3) > 1))


if __name__ == '__main__':
    print(distance (4, 5, 3))