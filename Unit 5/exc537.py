def chocolate_maker(small, big, x):
    maximum_size = small + big * 5
    dif = 0
    count = 0
    if maximum_size == x:
        return True
    if maximum_size > x:
        dif = maximum_size -x
        if dif / 5 <= big and dif % 5 == 0:
            return True
        if dif > 5:
            count = dif / 5
            if count > big:
                count = big
            dif -= 5 * count
        if small + (big - count) * 5 == x:
            return True
        if small + (big - count) * 5 > x and dif <= small:
            return True
    return False


if __name__ == '__main__':
    print(chocolate_maker(3, 2, 10))
