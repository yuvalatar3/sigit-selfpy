def is_valid_input(letter_guessed):
    """
    Checks if the passed parameter is a valid guess for the program or not.
    :param letter_guessed: The input the user entered.
    :type letter_guessed: str
    :return: True if the passed parameter is valid, otherwise returns False.
    :rtype: bool
    """
    return not(len(letter_guessed) > 1 or (not letter_guessed.isalpha()))


def main():
    guessed_letter = input("Guess a letter:")
    valid = is_valid_input(guessed_letter)
    print(valid)


if __name__ == '__main__':
    main()
    