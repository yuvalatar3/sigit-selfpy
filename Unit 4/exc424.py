import calendar

# Assuming that the input is valid.
def exc424():
    date = input("Enter a date: ")
    current_day = calendar.weekday(int(date.split('/')[2]), int(date.split('/')[1]), int(date.split('/')[0]))
    if current_day == 0:
        print("Monday")
    elif current_day == 1:
        print("Tuesday")
    elif current_day == 2:
        print("Wednesday")
    elif current_day == 3:
        print("Thursday")
    elif current_day == 4:
        print("Friday")
    elif current_day == 5:
        print("Saturday")
    elif current_day == 6:
        print("Sunday")


if __name__ == '__main__':
    exc424()
