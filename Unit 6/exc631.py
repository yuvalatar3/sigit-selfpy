def are_lists_equal(list1, list2):
    """
    Checks if all the elements are equals in the lists
    :param list1: Collection of integers and floats values.
    :param list2: Collection of integers and floats values.
    :type list1: list
    :type list2: list
    :return: True if the elements are equals in the lists, otherwise False.
    :rtype: bool
    """
    list1.sort()
    list2.sort()
    return list1 == list2


def main():
    list1 = [0.6, 1, 2, 3]
    list2 = [3, 2, 0.6, 1]
    list3 = [9, 0, 5, 10.5]
    print(are_lists_equal(list1, list2))


if __name__ == '__main__':
    main()
