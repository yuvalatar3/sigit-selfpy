def extend_list_x(list_x, list_y):
    """
    Puts all the y list element at the start of the x list.
    :param list_x: Collection which contains values.
    :param list_y:Collection which contains values.
    :type list_x: list
    :type list_y: list
    :return: merged list which contain at start the y list and then the x list.
    :rtype: list
    """
    list_z = list_x
    list_x = []
    for item in list_y:
        list_x.append(item)
    for item in list_z:
        list_x.append(item)
    return list_x


def main():
    x = [4, 5, 6]
    y = [1, 2, 3]
    print(extend_list_x(x, y))


if __name__ == '__main__':
    main()
