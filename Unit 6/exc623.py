def format_list(my_list):
    """
    Gets all the strings in the parity indexes and puts them in one string.
    In addition, it adds to this string the last object in the list as well.
    :param my_list: Collection of Strings.
    :type my_list: list
    :return: String which contain all the Strings in the list in the parity indexes and the last str instance
    in passed list
    :rtype: str
    """
    best_string = ', '.join(my_list[::2])
    best_string += ' and '
    best_string += ''.join(my_list[-1])
    return best_string


def main():
    print(format_list(["hydrogen", "helium", "lithium", "beryllium", "boron", "magnesium"]))


if __name__ == '__main__':
    main()
