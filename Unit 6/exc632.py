def longest(my_list):
    """
    Checks what is the longest element in the list.
    :param my_list: Collection of Strings
    :type my_list: list
    :return: The longest String in the list
    :rtype: str
    """
    best_lst = sorted(my_list, key=len)
    return best_lst[-1]


def main():
    list1 = ["111", "234", "2000", "goru", "birthday", "09"]
    print(longest(list1))


if __name__ == '__main__':
    main()
