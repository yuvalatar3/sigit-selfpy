def check_valid_input(letter_guessed, old_letters_guessed):
    """
    Checks if the passed parameter is a valid guess for the program or not.
    :param letter_guessed: The input the user entered.
    :param old_letters_guessed: All the attempts the user had ever tried during the game.
    :type letter_guessed: str
    :type old_letters_guessed: list
    :return: True if the passed parameter is valid, otherwise returns False.
    :rtype: bool
    """
    return not (len(letter_guessed) > 1 or (not letter_guessed.isalpha()) or (letter_guessed.lower() in old_letters_guessed))


def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    """
    Handles the cases when user enters new guesses
    :param letter_guessed: The input the user entered.
    :param old_letters_guessed: All the attempts the user had ever tried during the game.
    :type letter_guessed: str
    :type old_letters_guessed: list
    :return: True if the guess is valid, otherwise False.
    """
    is_valid_guess = check_valid_input(letter_guessed, old_letters_guessed)
    if is_valid_guess:
        old_letters_guessed.append(letter_guessed)
        return True
    else:
        print("X")
        new_list = sorted(old_letters_guessed)
        print(" -> ".join(new_list))
        return False


def main():
    old_letters = ['a', 'p', 'c', 'f']
    print(try_update_letter_guessed('A', old_letters))
    print(try_update_letter_guessed('s', old_letters))
    print(old_letters)
    print(try_update_letter_guessed('$', old_letters))
    print(try_update_letter_guessed('d', old_letters))
    print(old_letters)

if __name__ == '__main__':
    main()
