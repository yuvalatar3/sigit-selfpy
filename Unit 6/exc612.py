def shift_left(my_list):
    """
    Shifting left all the values in passed collection.
    :param my_list: Collection of 3 values.
    :type my_list: list
    :return: Values collection as the passed parameter after shifted left all the values in it.
    :rtype: list
    """
    my_list[0], my_list[1], my_list[2] = my_list[1], my_list[2], my_list[0]
    return my_list


def main():
    print(shift_left(['monkey', 2.0, 1]))


if __name__ == '__main__':
    main()
