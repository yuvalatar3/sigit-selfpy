def mult_tuple(tuple1, tuple2):
    """
    Creates all the available pairs and insert them to one tuple.
    :param tuple1:  Tuple with values.
    :param tuple2: Tuple with values.
    :return: Tuple with all possible pairs.
    :rtype: tuple
    """
    best_tuple = ()
    for item in tuple1:
        for item2 in tuple2:
            new_one = item, item2
            best_tuple = (*best_tuple, new_one)
            new_one = item2, item
            best_tuple = (*best_tuple, new_one)
    return best_tuple


def main():
    first_tuple = (1, 2, 3)
    second_tuple = (4, 5, 6)
    print(mult_tuple(first_tuple, second_tuple))


if __name__ == '__main__':
    main()
