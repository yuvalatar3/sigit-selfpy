def count_chars(my_str):
    """
    Checks how many times all the letters appears in a certain String.
    :param my_str: Some String value
    :type my_str: String
    :return: Collection which contains information about the amount of times each letter appears in the String.
    :rtype: dict
    """
    best_dict_ever = {}
    for letter in my_str:
        if letter != ' ':
            best_dict_ever[letter] = my_str.count(letter)
    return best_dict_ever


def main():
    magic_str = "abra cadabra"
    print(count_chars(magic_str))


if __name__ == '__main__':
    main()
