def main():
    ultra_dict = {'first_name': 'Mariah', 'last_name': 'Carey', 'birth_date': '23.09.1970', 'hobbies': ['Sing', 'Compose', 'Act']}
    operation = int(input("Select the method you want to commit: "))
    if operation == 1:
        print("The last name of Mariah is %s" % ultra_dict['last_name'])
    if operation == 2:
        print("Mariah was born in the %s month" % ultra_dict['birth_date'].split('.')[1])
    if operation == 3:
        print("Mariah has %d hobbies." % len(ultra_dict['hobbies']))
    if operation == 4:
        print("The last hobby Mariah has is: %s" % ultra_dict['hobbies'][-1])
    if operation == 5:
        ultra_dict['hobbies'].append("Cooking")
        print("The updated hobbies now, are: ", ultra_dict['hobbies'])
    if operation == 6:
        str_tup = ultra_dict['birth_date'].split('.')
        int_tup = (int(str_tup[0]), int(str_tup[1]), int(str_tup[2]))
        print(int_tup)
    if operation == 7:
        ultra_dict["age"] = 52
        print("Maria is %d years old." % ultra_dict['age'])


if __name__ == '__main__':
    main()
