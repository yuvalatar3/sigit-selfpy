def sort_prices(list_of_tuples):
    """
    Sort the given list by the items' prices.
    :param list_of_tuples: list which contains tuples of items and their price
    :type list_of_tuples: list
    :return: Sorted list of tuples
    :rtype: list
    """
    count = 0
    for item in list_of_tuples:
        count2 = 0
        for item2 in list_of_tuples:
            if float(item[1]) > float(item2[1]):
                list_of_tuples[count], list_of_tuples[count2] = list_of_tuples[count2], list_of_tuples[count]
            count2 += 1
        count += 1
    return list_of_tuples


def main():
    products = [('milk', '5.5'), ('candy', '2.5'), ('bread', '9.0')]
    print(sort_prices(products))


if __name__ == '__main__':
    main()
