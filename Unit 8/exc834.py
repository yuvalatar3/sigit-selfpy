def inverse_dict(my_dict):
    """
    Generate a reverse dictionary for the given one, which all the values becomes keys and all the keys become values.
    Takes care cases when some keys have the same value and put them in the same new list values in the dictionary.
    In addition, the function sorts the values in every list which placed in the value place in the new dictionary.
    :param my_dict: Dictionary with Strings values.
    :type my_dict: dict
    :return: Reversed dictionary.
    :rtype: dict
    """
    best_dict = {}
    for item in my_dict.items():
        if item[1] not in best_dict:
            best_dict[item[1]] = [item[0]]
        else:
            best_dict[item[1]].append(item[0])
    for value in best_dict.values():
        value.sort()
    return best_dict


def main():
    course_dict = {'I': 3, 'love': 3, 'self.py!': 2}
    print(inverse_dict(course_dict))


if __name__ == '__main__':
    main()
