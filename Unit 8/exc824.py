def sort_anagrams(list_of_strings):
    """
    Sort all the Strings in list to be in the list they belong to.
    Match groups by anagrams.
    :param list_of_strings: Values collection of all those anagrams lists.
    :type list_of_strings: String
    :return: Values collection of all those anagrams lists.
    :type: list.
    """
    best_lst = []
    for word in list_of_strings:
        found = False
        for lst in best_lst:
            for word3 in lst:
                if word3 == word:
                    found = True
        if not found:
            new_lst = []
            new_lst.append(word)
            for word2 in list_of_strings:
                flag = True
                if word2 != word:
                    for letter in word2:
                        if letter not in word:
                            flag = False
                    if flag is True:
                        new_lst.append(word2)
            best_lst.append(new_lst)
    return best_lst



def main():
    list_of_words = ['deltas', 'retainers', 'desalt', 'pants', 'slated', 'generating', 'ternaries', 'smelters', 'termless', 'salted', 'staled', 'greatening', 'lasted', 'resmelts']
    print(sort_anagrams(list_of_words))


if __name__ == '__main__':
    main()
