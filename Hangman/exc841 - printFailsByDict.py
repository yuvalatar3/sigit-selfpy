HANGMAN_PHOTOS = {
    0: """    x-------x""",
    1: """    x-------x
    |
    |
    |
    |
    |""",
    2: """    x-------x
    |       |
    |       0
    |
    |
    |""",
    3: """    x-------x
    |       |
    |       0
    |       |
    |
    |""",
    4: r"""    x-------x
    |       |
    |       0
    |      /|\
    |
    |""",
    5: r"""    x-------x
    |       |
    |       0
    |      /|\
    |      /
    |""",
    6: r"""    x-------x
    |       |
    |       0
    |      /|\
    |      / \
    |"""}


def print_hangman(num_of_tries):
    """
    Prints the current player status according to the num of tries.
    :param num_of_tries: The amount of times a player failed in his guess.
    :type num_of_tries: int
    :return: None.
    :type: None.
    """
    print(HANGMAN_PHOTOS[num_of_tries])


def main():
    num_of_tries = 6
    print_hangman(num_of_tries)


if __name__ == '__main__':
    main()
