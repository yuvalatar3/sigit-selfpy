def main():
    print("""    x-------x""")
    print("""    x-------x
    |
    |
    |
    |
    |""")
    print("""    x-------x
    |       |
    |       0
    |
    |
    |""")
    print("""    x-------x
    |       |
    |       0
    |       |
    |
    |""")
    print(r"""    x-------x
    |       |
    |       0
    |      /|\
    |
    |""")
    print(r"""    x-------x
    |       |
    |       0
    |      /|\
    |      /
    |""")
    print(r"""    x-------x
    |       |
    |       0
    |      /|\
    |      / \
    |""")

if __name__ == '__main__':
    main()