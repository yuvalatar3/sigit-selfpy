def exc431():
    guessed_letter = input("Guess a letter:")

    if len(guessed_letter) > 1 and (not guessed_letter.isalpha()):
        print("E3")
    elif len(guessed_letter) > 1:
        print("E1")
    elif not guessed_letter.isalpha():
        print("E2")
    else:
        print(guessed_letter.lower())

if __name__ == '__main__':
    exc431()