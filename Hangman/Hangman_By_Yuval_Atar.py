MAX_TRIES = 6


def hangman_opening():
    """
    Prints the opening screen for the game.
    :return: None.
    """
    HANGMAN_ASCII_ART = """Welcome to the game Hangman\n       _    _                                         
      | |  | |                                        
      | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
      |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
      | |  | | (_| | | | | (_| | | | | | | (_| | | | |
      |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                           __/ |                      
                          |___/"""
    print(HANGMAN_ASCII_ART, "\n", MAX_TRIES)


def check_valid_input(letter_guessed, old_letters_guessed):
    """
    Checks if the passed parameter is a valid guess for the program or not.
    :param letter_guessed: The input the user entered.
    :param old_letters_guessed: All the attempts the user had ever tried during the game.
    :type letter_guessed: str
    :type old_letters_guessed: list
    :return: True if the passed parameter is valid, otherwise returns False.
    :rtype: bool
    """
    return not (len(letter_guessed) > 1 or (not letter_guessed.isalpha()) or (letter_guessed.lower() in old_letters_guessed))


def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    """
    Handles the cases when user enters new guesses
    :param letter_guessed: The input the user entered.
    :param old_letters_guessed: All the attempts the user had ever tried during the game.
    :type letter_guessed: str
    :type old_letters_guessed: list
    :return: True if the guess is valid, otherwise False.
    """
    is_valid_guess = check_valid_input(letter_guessed, old_letters_guessed)
    if is_valid_guess:
        old_letters_guessed.append(letter_guessed)
        return True
    else:
        print("X")
        new_list = sorted(old_letters_guessed)
        print(" -> ".join(new_list))
        return False


def show_hidden_word(secret_word, old_letters_guessed):
    """
    Creates a String which display the user when he did correct moves so far.
    :param secret_word: The value the player need to guess.
    :param old_letters_guessed: All the attempts the user had ever tried during the game.
    :type secret_word: String
    :type old_letters_guessed: list
    :return: The current status of the player.
    :rtype: String
    """
    status_lst = []
    for i in range(len(secret_word)):
        status_lst.append('_')
    for letter in old_letters_guessed:
        if letter in secret_word:
            idx = 0
            for letter2 in secret_word:
                if letter == letter2:
                    status_lst[idx] = letter
                idx += 1
    return " ".join(status_lst)


def check_win(secret_word, old_letters_guessed):
    """
    Checks if the player succeed to win the game session
    Note: The first implementation version of it was less efficient so we decided to use an already wrote function.
    :param secret_word: The value the player need to guess.
    :param old_letters_guessed: All the attempts the user had ever tried during the game.
    :type secret_word: String
    :type old_letters_guessed: list
    :return: True if the player wins, otherwise False
    """
    return not('_' in show_hidden_word(secret_word, old_letters_guessed))


def print_hangman(num_of_tries):
    """
    Prints the current player status according to the num of tries.
    :param num_of_tries: The amount of times a player failed in his guess.
    :type num_of_tries: int
    :return: None.
    :type: None.
    """
    HANGMAN_PHOTOS = {
        0: """    x-------x""",
        1: """    x-------x
    |
    |
    |
    |
    |""",
        2: """    x-------x
    |       |
    |       0
    |
    |
    |""",
        3: """    x-------x
    |       |
    |       0
    |       |
    |
    |""",
        4: r"""    x-------x
    |       |
    |       0
    |      /|\
    |
    |""",
        5: r"""    x-------x
    |       |
    |       0
    |      /|\
    |      /
    |""",
        6: r"""    x-------x
    |       |
    |       0
    |      /|\
    |      / \
    |"""}
    print(HANGMAN_PHOTOS[num_of_tries])


def choose_word(file_path, index):
    """
    Chooses a word from the file located in the file path passed parameter,  based by passed index parameter value.
    :param file_path: The file path where all the words are locates.
    :param index: The index of the word in the file.
    :type file_path: String
    :type index: int
    :return: The amount of words in the words.txt, without duplicates, and the selected word from the file.
    :rtype: tuple
    """
    f = open(file_path, 'r')
    words = f.read()
    all_words = words.split(' ')
    count = 1
    selected_word = ""
    while index > len(all_words):
        index -= len(all_words)
    for word in all_words:
        if count == index:
            selected_word = word
        count += 1
    for word in all_words:
        found = 0
        for word2 in all_words:
            if word == word2:
                found +=1
                if found > 1:
                    all_words.remove(word)
    return len(all_words), selected_word


def main():
    old_letters_guessed = []
    num_of_tries = 0
    hangman_opening()
    print("\n")

    path = input("Enter your file location here: ")
    position = int(input("Where is the hidden word locate ?"))
    print("\n")
    secret_word = choose_word(path, position)[1]

    print("Lets Go !\nThis is your status right now:")
    print_hangman(num_of_tries)
    print("\n")

    print("This is the word that you chose: ", show_hidden_word(secret_word, old_letters_guessed), "\n")

    while num_of_tries < MAX_TRIES:
        current_letter = input("Enter an English letter here: ").lower()
        available = try_update_letter_guessed(current_letter, old_letters_guessed)
        while not available:
            current_letter = input("Please enter another letter here: ").lower()
            available = try_update_letter_guessed(current_letter, old_letters_guessed)
        if current_letter not in secret_word:
            print(":(")
            num_of_tries += 1
            print_hangman(num_of_tries)
            print("\n")
            print(' '.join(show_hidden_word(secret_word, old_letters_guessed)))
            continue
        print(' '.join(show_hidden_word(secret_word, old_letters_guessed)))
        if check_win(secret_word, old_letters_guessed):
            print("WIN")
            break
    if num_of_tries == MAX_TRIES:
        print("LOSE")


if __name__ == '__main__':
    main()
