def who_is_missing(file_name):
    """
    Checks what is the missing number in the row.
    :param file_name: A path to a file.
    :type file_name: String
    :return: None.
    :rtype: None.
    """
    nums_file = open(file_name, 'r')
    data = nums_file.read()
    nums_file.close()
    all_str_nums = data.split(',')
    all_n = []
    for n in all_str_nums:
        all_n.append(int(n))
    last = 0
    for num in all_n:
        if num - last != 1:
            last = num
            break
        last += 1
    f = open("found.txt", 'w')
    f.write(str(last - 1))
    f.close()


def main():
    who_is_missing("check1.txt")


if __name__ == '__main__':
    main()

