def my_mp4_playlist(file_path, new_song):
    """
    Update the songs lists.
    :param file_path: Some path to a file.
    :param new_song: New song to add to the file.
    :type file_path: String
    :type new_song: String
    :return: None.
    :rtype: None.
    """
    f = open(file_path, 'r')
    data = f.read()
    f.close()
    new_data = data.split("\n")[:2]
    new_data.append("%s;%s;%s;" % (new_song, "Unknown", "4:15"))
    new_data += data.split("\n")[3:]
    fetched_data = ""
    for element in new_data:
        element_data = element.split(';')
        fetched_data += "%s;%s;%s;\n" % (element_data[0], element_data[1], element_data[2])
    f = open(file_path, 'w')
    f.write(fetched_data)
    f.close()


def main():
    my_mp4_playlist("check1.txt", "Python Love Story")


if __name__ == '__main__':
    main()
