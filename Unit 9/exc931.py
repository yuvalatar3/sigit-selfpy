def my_mp3_list(file_path):
    """
    Gathering information about the songs
    :param file_path: Some path to a file.
    :type file_path: String
    :return: Gathered information.
    :rtype: tuple
    """
    f = open(file_path, 'r')
    data = f.read()
    f.close()
    data_lines = data.split("\n")
    gathered_info = []
    longest = 0
    longest_name = ""
    best_name = ""
    biggest_counter = 0
    for line in data_lines:
        gathered_info.append(line.split(";"))
    for element in gathered_info:
        minutes, secs = element[2].split(":")
        length = int(minutes) * 60 + int(secs)
        if longest < length:
            longest = length
            longest_name = element[0]
    songs_amount = len(gathered_info)
    for element in gathered_info:
        current_name = element[1]
        current_counter = 0
        for element2 in gathered_info:
            if element2[1] == current_name:
                current_counter += 1
                if current_counter > biggest_counter:
                    biggest_counter = current_counter
                    best_name = current_name
    return longest_name, songs_amount, best_name


def main():
    print(my_mp3_list("check1.txt"))


if __name__ == '__main__':
    main()
