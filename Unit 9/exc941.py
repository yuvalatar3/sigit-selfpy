def choose_word(file_path, index):
    """
    Chooses a word from the file located in the file path passed parameter,  based by passed index parameter value.
    :param file_path: The file path where all the words are locates.
    :param index: The index of the word in the file.
    :type file_path: String
    :type index: int
    :return: The amount of words in the words.txt, without duplicates, and the selected word from the file.
    :rtype: tuple
    """
    f = open(file_path, 'r')
    words = f.read()
    all_words = words.split(' ')
    count = 1
    selected_word = ""
    while index > len(all_words):
        index -= len(all_words)
    for word in all_words:
        if count == index:
            selected_word = word
        count += 1
    for word in all_words:
        found = 0
        for word2 in all_words:
            if word == word2:
                found +=1
                if found > 1:
                    all_words.remove(word)
    return len(all_words), selected_word


def main():
    print(choose_word("words.txt", 3))
    print(choose_word("words.txt", 15))


if __name__ == '__main__':
    main()
