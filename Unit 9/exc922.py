def copy_file_content(source, destination):
    """
    Copies all the data from the source file to the destination file.
    :param source: Any file path.
    :param destination: Any file path.
    :type source: String
    :type destination: String
    :return: None.
    :rtype: None.
    """
    src_file = open(source, 'r')
    data = src_file.read()
    src_file.close()
    destination_file = open(destination, 'w')
    destination_file.write(data)
    destination_file.close()


def main():
    copy_file_content("check1.txt", "check2.txt")


if __name__ == '__main__':
    main()
