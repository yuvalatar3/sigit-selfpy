def are_files_equal(file1, file2):
    """
    Checks if the files contains the same data or not.
    :param file1: Some path to a file.
    :param file2: Some path to a file.
    :type file1: file
    :type file2: file
    :return: True if the files contains the same data, otherwise returns False.
    :rtype: bool
    """
    first_file = open(file1, 'r')
    first_data = first_file.read()
    first_file.close()
    second_file = open(file2, 'r')
    second_data = second_file.read()
    second_file.close()
    return first_data == second_data


def main():
    print(are_files_equal("check1.txt", "check2.txt"))


if __name__ == '__main__':
    main()
