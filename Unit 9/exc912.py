def main():
    file_path = input("Enter your file path here: ")
    current_file = open(file_path, 'r')
    operation = input("Decide what you want to do: ")
    if operation == "sort":
        best_list = []
        for line in current_file:
            cur_lst = line.split(' ')
            for word in cur_lst:
                if word not in best_list:
                    if "\n" in word:
                        best_list.append(word[:-1])
                    else:
                        best_list.append(word)
        best_list.sort()
        print(best_list)
    if operation == "rev":
        for line in current_file:
            print(line[::-1])
    if operation == "last":
        lines_number = int(input("How many lines you want to show "))
        lines = current_file.read().split("\n")
        for i in range(lines_number):
            print(lines[len(lines) - lines_number + i])


if __name__ == '__main__':
    main()
